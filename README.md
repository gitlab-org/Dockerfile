# [DEPRECATED] A collection of `Dockerfile` templates

This was GitLab's default collection of instance-level `Dockerfile` templates.

## Contribution

Further contributions for the default Dockerfile templates can be made in
[the GitLab main repository under vendor/Dockerfile](https://gitlab.com/gitlab-org/gitlab/-/tree/master/vendor/Dockerfile).
